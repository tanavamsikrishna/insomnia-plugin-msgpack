// For help writing plugins, visit the documentation to get started:
//   https://support.insomnia.rest/article/26-plugins

const msgpack = require('msgpack-lite');

module.exports.responseHooks = [
  context => {
    const contentType = context.response.getHeader('Content-Type');
    const msgpackContentTypes = ['application/msgpack', 'application/x-msgpack']
    if (msgpackContentTypes.includes(contentType)) {
      const body = context.response.getBody();
      const data = msgpack.decode(body)
      context.response.setBody(JSON.stringify(data));
    }
  }
]