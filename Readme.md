# MessagePack responses plugin for [Insomnia REST Client](https://insomnia.rest)

### About

Based on the value of the header `Content-Type` in a HTTP(s) response, this plugin presents a *msgpack* body as a *JSON* document. 

If the value of the response header `Content-Type` is either `application/msgpack` or `application/x-msgpack`, this plugin replaces the (and only) response body, with the corresponding *JSON* document.

